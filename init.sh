#!/bin/bash

# Init script for multi-file user crontab.
#
# It updates the crontab so that it's populated from files in a directory pretty
# much like Vixie Cron's "/etc/cron.d" directory.
# It auto schedules itself to run every minute (see create_init_crontab_part())
# so that any changes in files are quickly taken into account.
# Every minute checks avoid relying on inotify.
#
# Usage: init.sh

set -euo pipefail

readonly PROGNAME="$(basename -- "$0")"
readonly PROGDIR="$(readlink -m -- "$(dirname -- "$0")")"
readonly ARGS="$@"


# Configuration:

###
 # The crontab header file, which will be prepended to the final generated crontab.
 ##
readonly CRONTAB_HEADER_FILEPATH="${PROGDIR}/crontab-header"

###
 # The file to store a backup of the previous crontab file.
 ##
readonly PREVIOUS_CRONTAB_FILEPATH="${PROGDIR}/crontab.backup"

###
 # The cronfiles directory, where all usercron files are to be put.
 ##
readonly CRONFILES_DIRPATH="${PROGDIR}/cron.d"

# /Configuration


# Environnement:
readonly crontab_bin="$(which crontab)"
readonly hash_bin="$(which sha256sum || which sha1sum || which md5sum)"
# /Environnement


# Functions:

###
 # Gets the current real crontab (of current user) from system.
 #
 # If current user has no crontab, returns an empty string.
 ##
function get_current_crontab() {
    "${crontab_bin}" -l 2> /dev/null || echo ""
}

###
 # Gets a convenient default crontab header that will be prepended to final
 # generated crontab.
 ##
function get_default_crontab_header() {
    cat <<EOT
# This crontab is managed by "${PROGDIR}"
#
# Any changes will be overwritten by "${PROGDIR}/${PROGNAME}".

EOT
}

###
 # Creates the "dynamic" part of the final generated crontab (the one built from
 # files of the cronfiles directory).
 ##
function create_dynamic_crontab_part() {
    # Find, in the cronfiles directory (first depth level):
    # * Files or symbolic links
    # * Not empty
    # * Existing (skips broken symlinks)
    # * Not containing a dot (".") in their name (conform to Vixie Cron's behaviour)
    # Uses "\0" caracter as separator to support file names with spaces
    find \
        "${CRONFILES_DIRPATH}/" \
        -maxdepth 1 \
        \( -xtype f -o -xtype l \) \
        ! -empty \
        ! -iname "*.*" \
        -exec test -e {} \; \
        -print0 \
    | sort -z \
    | xargs \
        -0 \
        --no-run-if-empty \
        -L 1 \
        bash -c 'echo -e "# File \"$(basename -- "$0")\":" ; cat "$0" ; echo -e "# /File \"$(basename -- "$0")\"\n"'
}

###
 # Creates the "init" part of the final generated crontab (the one that makes
 # sure the current script is executed, and thus the crontab is updated after
 # changes in ronfiles directory).
 ##
function create_init_crontab_part() {
    cat <<EOT
# multifile_user_crontab's init script (remove to disable automatic updates of the crontab)
        *       *       *       *       *       "${PROGDIR}/${PROGNAME}" > /dev/null
EOT
}

###
 # Creates the final generated crontab (is assembles the header, the init and
 # the dynamic parts).
 ##
function create_final_crontab() {
    local header_part_filepath="$1"
    local init_part="$2"
    local dynamic_part="$3"

    cat "${header_part_filepath}"
    echo ''
    echo "${init_part}"
    echo ''
    echo "${dynamic_part}"
}

###
 # Creates a backup of the current crontab to a file.
 # It adds an header with date and time of backup.
 ##
function backup_current_crontab() {
    echo "# Backup of ${LOGNAME}'s crontab performed on: $(date --utc +'%F %T %Z')\n" > "${PREVIOUS_CRONTAB_FILEPATH}"
    get_current_crontab >> "${PREVIOUS_CRONTAB_FILEPATH}"
}

# /Functions


# Checks:
if [ ! -x "${crontab_bin}" ]; then
    (>&2 echo "\"crontab\" command is missing or is not executable. Exiting.")
    exit 1
fi

if [ ! -x "${hash_bin}" ]; then
    (>&2 echo "No hashing command found (or is not executable). Exiting.")
    exit 1
fi

if [ ! -e "${CRONFILES_DIRPATH}" ]; then
    mkdir --parents "${CRONFILES_DIRPATH}"
fi

if [ ! -e "${CRONTAB_HEADER_FILEPATH}" ]; then
    get_default_crontab_header > "${CRONTAB_HEADER_FILEPATH}"
fi

# /Checks


# Process:

# Get current crontab's hash
current_crontab_hash="$(get_current_crontab | "${hash_bin}")"

# Create the crontab as per the header, init and "cron.d" files
final_crontab="$(create_final_crontab "${CRONTAB_HEADER_FILEPATH}" "$(create_init_crontab_part)" "$(create_dynamic_crontab_part)")"

final_crontab_hash="$(echo "${final_crontab}" | "${hash_bin}")"

if [ "${current_crontab_hash}" != "${final_crontab_hash}" ]; then
    echo "Current user's crontab does not match what the files in \"${CRONFILES_DIRPATH}\" defines: Will update it..."
    backup_current_crontab
    echo "${final_crontab}" | crontab -
else
    echo "Current user's crontab is up-to-date"
fi
