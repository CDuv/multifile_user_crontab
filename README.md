Multi-file user crontab
=======================

This project allows a user to manage it's crontab via multiple files, just like
Vixie Cron's `/etc/cron.d` directory.

Why
---

I find it more convenient to add/delete files than modifying what `crontab -e`
sends. It's used by various GNU Linux distributions at system-level with the
`/etc/cron.d` directory so it should work for an user too.

What it does
------------

The script assembles the desired/final crontab by concatenating all files and
overwrite user's real crontab with it.

How it works
------------

The script file schedules itself so that added/updated/deleted files are taken
into account.
It's a "every-minute schedule": it does not rely on a file-changed detection
system like inotify.

Installation
------------

To install this project, get `init.sh`, place it in a directory (where you have
write access as it will create `crontab-header` file and `cron.d` directory)
and execute it.

⚠️ Warning: **It will completely wipe your existing crontab.**

*(A copy will be made as `crontab.backup` in the installation directory)*

```Shell
# Installation directory:
mkdir ~/multifile_crontab
cd !*

# Fetch (using you preferred way):
wget https://gitlab.com/CDuv/multifile_user_crontab/-/raw/main/init.sh
git clone https://gitlab.com/CDuv/multifile_user_crontab.git .

# Installation:
chmod u+x init.sh
./init.sh

# Usage:
# Simply add files to `cron.d` directory:
echo "* * * * * echo 'Hi' >> /tmp/test-multifile_user_crontab.txt" > "./cron.d/example"
```

Internal technical stuff
------------------------

### Cronfiles (id. files in `cron.d` directory)

To be taken into account (id. be part of the final crontab) the file must:

* be a file or a symbolic link
* be not empty
* exist (skips broken symlinks)
* not contain a dot (".") in their name (conform to Vixie Cron's behaviour)

Source
------

Sources are available at: [https://gitlab.com/CDuv/multifile_user_crontab](https://gitlab.com/CDuv/multifile_user_crontab)
